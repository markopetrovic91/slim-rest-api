<?php

$app->route(['GET', 'PUT', 'DELETE'], '/articles[/{id}]', \App\Controllers\ArticleController::class);
$app->route(['POST'], '/articles', \App\Controllers\ArticleController::class);
