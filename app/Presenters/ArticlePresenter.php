<?php

namespace App\Presenters;

class ArticlePresenter extends BasePresenter
{
    public function format()
    {
        return [
            'title' => $this->data->title,
            'body' => $this->data->body
        ];
    }
}
